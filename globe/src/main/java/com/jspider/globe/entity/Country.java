package com.jspider.globe.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

@Entity
@Table(name="country")
public class Country implements Serializable{
	
	@Id
	@GenericGenerator(name="key_auto",strategy="increment")
	@GeneratedValue(generator="key_auto")
	@Column(name = "id")
	private int id;
	
	@Column(name = "Name")
	private String Name;
	
	@Column(name = "Country")
	private String Country;
	
	@Column(name = "Area")
	private String Area;
	
	@Column(name = "Population")
	private String Population;
	
	@Column(name = "Capital")
	private String Capital;
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return Name;
	}
	public void setName(String name) {
		Name = name;
	}
	public String getCountry() {
		return Country;
	}
	public void setCountry(String country) {
		Country = country;
	}
	public String getArea() {
		return Area;
	}
	public void setArea(String area) {
		Area = area;
	}
	public String getPopulation() {
		return Population;
	}
	public void setPopulation(String population) {
		Population = population;
	}
	public String getCapital() {
		return Capital;
	}
	public void setCapital(String capital) {
		Capital = capital;
	}
	@Override
	public String toString() {
		return "Country [id=" + id + ", Name=" + Name + ", Country=" + Country + ", Area=" + Area + ", Population="
				+ Population + ", Capital=" + Capital + "]";
	}

}
