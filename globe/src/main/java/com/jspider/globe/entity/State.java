package com.jspider.globe.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

@Entity
@Table(name="state")
public class State implements Serializable {
	
	@Id
	@GenericGenerator(name="key_auto",strategy="increment")
	@GeneratedValue(generator="key_auto")
	@Column(name = "id")
	private int id;
	
	@Column(name = "CountryName")
	private String CountryName;
	
	@Column(name = "Name")
	private String Name;
	
	@Column(name = "Capital")
	private String Capital;
	
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return Name;
	}
	public void setName(String name) {
		Name = name;
	}
	

	public String getCapital() {
		return Capital;
	}
	public void setCapital(String capital) {
		Capital = capital;
	}
	
	@Override
	public String toString() {
		return "State [id=" + id + ", CountryName=" + CountryName + ", Name=" + Name + ", Capital=" + Capital + "]";
	}
	public String getCountryName() {
		return CountryName;
	}
	public void setCountryName(String countryName) {
		CountryName = countryName;
	}

}
