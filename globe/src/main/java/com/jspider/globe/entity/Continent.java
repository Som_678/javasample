package com.jspider.globe.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

@Entity
@Table(name="continent")
public class Continent implements Serializable {
	
	@Id
	@GenericGenerator(name="key_auto",strategy="increment")
	@GeneratedValue(generator="key_auto")
	
	@Column(name = "id")
	private int id;
	
	@Column(name = "Name")
    private String Name;
	
	@Column(name = "Area")
	private String Area;
	
	@Column(name = "Population")
	private String Population;
	public String getName() {
		return Name;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getArea() {
		return Area;
	}
	public void setArea(String area) {
		Area = area;
	}
	public String getPopulation() {
		return Population;
	}
	public void setPopulation(String population) {
		Population = population;
	}
	public void setName(String name) {
		Name = name;
	}
	@Override
	public String toString() {
		return "Continent [id=" + id + ", Name=" + Name + ", Area=" + Area + ", Population=" + Population + "]";
	}
}