package com.jspider.globe.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.jspider.globe.entity.Continent;
import com.jspider.globe.entity.Country;
import com.jspider.globe.entity.State;
import com.jspider.globe.service.Globeservice;


@Controller
@RequestMapping("/")
public class Globecontroller {
	
	
		@Autowired
		private Globeservice globeservice ;

		@RequestMapping("/saveContinentDetails")
		public ModelAndView saveContinentDetails(Continent continent) {
			System.out.println(continent);
			globeservice.saveContinentDetails(continent);
			return new ModelAndView("Success.jsp");
		}

		@RequestMapping("/saveCountryDetails")
		public ModelAndView saveCountryDetails(Country country) {
			System.out.println(country);
			globeservice.saveCountryDetails(country);
			return new ModelAndView("Success.jsp");
		}
			@RequestMapping("/saveStateDetails")
			public ModelAndView saveStateDetails(State state) {
				System.out.println(state);
				globeservice.saveStateDetails(state);
				return new ModelAndView("Success.jsp");
		}
	}



