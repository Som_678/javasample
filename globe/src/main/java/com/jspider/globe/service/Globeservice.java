package com.jspider.globe.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.jspider.globe.entity.Continent;
import com.jspider.globe.entity.Country;
import com.jspider.globe.entity.State;
import com.jspider.globe.repository.Globerepository;


@Service
public class Globeservice {
		
		@Autowired
		private Globerepository globerepository;
		
	public void saveContinentDetails(Continent continent) {
		System.out.println(continent);	
		globerepository.saveContinentDetails(continent);
		}
	
		public void saveCountryDetails(Country country) {
			System.out.println(country);
			globerepository.saveCountryDetails(country);
		}
		
		public void saveStateDetails(State state) {
			System.out.println(state);
			globerepository.saveStateDetails(state);
		}

}
