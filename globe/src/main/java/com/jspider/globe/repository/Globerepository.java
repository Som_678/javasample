package com.jspider.globe.repository;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;
import org.springframework.stereotype.Repository;

import com.jspider.globe.entity.Continent;
import com.jspider.globe.entity.Country;
import com.jspider.globe.entity.State;


@Repository
public class Globerepository {
	
//		private SessionFactory sessionFactory;
//		
		
		public void saveContinentDetails(Continent continent) {
			
			Configuration cfg=new Configuration();
			 cfg.configure();
			 SessionFactory sessionFactory = cfg.buildSessionFactory();
			 Session session=sessionFactory.openSession();
			 Transaction transaction= session.beginTransaction();
			 session.save(continent);
			 transaction.commit();
		}
		
			public void saveCountryDetails(Country country) {
			
			Configuration cfg=new Configuration();
			 cfg.configure();
			 SessionFactory sessionFactory = cfg.buildSessionFactory();
			 Session session=sessionFactory.openSession();
			 Transaction transaction= session.beginTransaction();
			 session.save(country);
			 transaction.commit();

}
public void saveStateDetails(State state) {
	
	Configuration cfg=new Configuration();
	 cfg.configure();
	 SessionFactory sessionFactory = cfg.buildSessionFactory();
	 Session session=sessionFactory.openSession();
	 Transaction transaction= session.beginTransaction();
	 session.save(state);
	 transaction.commit();
}
}
