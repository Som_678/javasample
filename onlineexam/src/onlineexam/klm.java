package onlineexam;

public class klm {
	
	String question = null;
	String[] answer =new String[5];

	public klm(String question, String[] answer) {
		super();
		this.question= question;
		this.answer[0] = answer[0];
		this.answer[1] = answer[1];
		this.answer[2] = answer[2];
		this.answer[3] = answer[3];
		this.answer[4] = answer[4];
	}
	public String getQues() {
		return question;
	}
	public void setQues(String ques) {
		this.question = ques;
	}
	public String[] getAns() {
		return answer;
	}
	public void setAns(String[] ans) {
		this.answer = ans;
	}
	@Override
	public String toString() {
		return "Questions [ques=" + question+ ", ans=" + answer+ "]";
	}

}
